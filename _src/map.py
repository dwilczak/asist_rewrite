from __future__ import annotations
import sys
from typing import Any, Generic, Optional, TypeVar, Union, overload

if sys.version_info < (3, 9):
    from typing import Iterable, Iterator, Sequence, List as list, Tuple as tuple
else:
    from collections.abc import Iterable, Iterator, Sequence

from .block import Block
from .layer import Layer
from .row import Row

__all__ = ["Map"]

Self = TypeVar("Self", bound="Map")
T = TypeVar("T")

TwoIndexes = Union[
    tuple[int, int],
    tuple[int, int, slice],
    tuple[int, slice, int],
    tuple[slice, int, int],
]
OneIndex = Union[
    int,
    tuple[int, slice],
    tuple[slice, int],
    tuple[int, int, slice],
    tuple[int, slice, int],
    tuple[slice, int, int],
]


class Map(Sequence[T], Generic[T]):
    """
    Contains a full map of blocks.

    Usage (like a list)
    --------------------
        Create a Map.
            >>> map = Map([
            ...     [
            ...         [Block.EMPTY, Block.WALL],
            ...         [Block.EMPTY, Block.WALL],
            ...     ],
            ...     [
            ...         [Block.EMPTY, Block.WALL],
            ...         [Block.EMPTY, Block.WALL],
            ...     ],
            ... ])
            >>> map
            Map([[[Block.EMPTY, Block.WALL], [Block.EMPTY, Block.WALL]], [[Block.EMPTY, Block.WALL], [Block.EMPTY, Block.WALL]]])

        Index a layer.
            >>> map[0]
            Layer([[Block.EMPTY, Block.WALL], [Block.EMPTY, Block.WALL]])

        Index a row.
            >>> map[0, 0]
            Row([Block.EMPTY, Block.WALL])

        Index a block.
            >>> map[0, 0, 0]
            Block.EMPTY

        Update a block.
            >>> map[0, 0, 0] = Block.WALL
            >>> map
            Map([[[Block.WALL, Block.WALL], [Block.EMPTY, Block.WALL]], [[Block.EMPTY, Block.WALL], [Block.EMPTY, Block.WALL]]])

        Iterate through the map.
            >>> for layer in map:
            ...     for row in layer:
            ...         for block in row:
            ...             print(block)
            ... 
            Block.WALL
            Block.WALL
            Block.EMPTY
            Block.WALL
            Block.EMPTY
            Block.WALL
            Block.EMPTY
            Block.WALL
    """
    _layers: list[Layer[T]]

    __slots__ = {
        "_layers":
            "A list of layers.",
    }

    def __init__(self: Self, iterable: Iterable[Iterable[T]]) -> None:
        self._layers = [Layer(layer) for layer in iterable]

    @overload
    def __getitem__(self: Self, index: tuple[int, int, int]) -> T: ...

    @overload
    def __getitem__(self: Self, index: TwoIndexes) -> Row[T]: ...

    @overload
    def __getitem__(self: Self, index: OneIndex) -> Layer[T]: ...

    @overload
    def __getitem__(
        self: Self, index: Union[slice, tuple[slice, slice], tuple[slice, slice, slice]]) -> Map[T]: ...

    def __getitem__(self, index):
        """Get an index or slice. Supports multi-indexing."""
        if isinstance(index, tuple) and len(index) not in (2, 3):
            raise TypeError(
                f"expected either map[layer], map[layer, row], or map[layer, row, column]")
        elif isinstance(index, tuple):
            layer, row, column = (*index, slice(None))[:3]
            if not isinstance(layer, slice):
                return self._layers[layer][row][column]
            elif isinstance(row, slice):
                if isinstance(column, slice):
                    return Map(
                        [
                            [
                                self._layers[i][j][k]
                                for k in range(len(self._layers[i][j]))[column]
                            ]
                            for j in range(len(self._layers[i]))[row]
                        ]
                        for i in range(len(self))[layer]
                    )
                else:
                    return Layer(
                        [
                            [
                                self._layers[i][j][k]
                                for k in range(len(self._layers[i][j]))[column]
                            ]
                            for j in range(len(self._layers[i]))[row]
                        ]
                        for i in range(len(self))[layer]
                    )
            else:
                if isinstance(column, slice):
                    return Layer(
                        [
                            self._layers[i][row][k]
                            for k in range(len(self._layers[i][row]))[column]
                        ]
                        for i in range(len(self))[layer]
                    )
                else:
                    return Row(
                        self._layers[i][row][column]
                        for i in range(len(self))[layer]
                    )
        elif isinstance(index, slice):
            return Map(self._layers[index])
        else:
            return self._layers[index]

    def __len__(self: Self) -> int:
        """The amount of layers in the map."""
        return len(self._layers)

    def __iter__(self: Self) -> Iterator[T]:
        """Generates all of the layers in the map."""
        return iter(self._layers)

    def __reversed__(self: Self) -> Iterator[T]:
        """Generates all of the layers in the map in reversed order."""
        return reversed(self._layers)

    def __repr__(self: Self) -> str:
        """Code representation of the map."""
        return f"{type(self).__name__}({[[row._blocks for row in layer] for layer in self._layers]})"

    def __setitem__(self: Self, index: tuple[int, int, int], value: T) -> None:
        """Update an index."""
        layer, row, column = index
        self._layers[layer][row][column] = value

    @property
    def shape(self: Self) -> tuple[int, int, int]:
        """Returns (depth, height, width)."""
        return (len(self._layers), len(self._layers[0], len(self._layers[0][0])))

    @property
    def size(self: Self) -> int:
        """Returns depth * height * width."""
        return len(self._layers) * len(self._layers[0] * len(self._layers[0][0]))
