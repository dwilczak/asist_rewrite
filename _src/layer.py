from __future__ import annotations

import sys
from typing import Any, Generic, Optional, TypeVar, Union, overload

if sys.version_info < (3, 9):
    from typing import Iterable, Iterator, Sequence, List as list, Tuple as tuple
else:
    from collections.abc import Iterable, Iterator, Sequence

from .block import Block
from .row import Row

__all__ = ["Layer"]

Self = TypeVar("Self", bound="Layer")
T = TypeVar("T")


class Layer(Sequence[T], Generic[T]):
    """
    Contains a layer of blocks.

    Usage (like a list)
    --------------------
        Create a Layer.
            >>> layer = Layer([
            ...     [Block.EMPTY, Block.WALL],
            ...     [Block.EMPTY, Block.WALL],
            ... ])
            >>> layer
            Layer([[Block.EMPTY, Block.WALL], [Block.EMPTY, Block.WALL]])

        Index a row.
            >>> layer[0]
            Row([Block.EMPTY, Block.WALL])

        Index a block.
            >>> layer[0, 0]
            Block.EMPTY

        Update a block.
            >>> layer[0, 0] = Block.WALL
            >>> layer

            Layer([[Block.WALL, Block.WALL], [Block.EMPTY, Block.WALL]])

        Iterate through the layer.
            >>> for row in layer:
            ...     for block in row:
            ...         print(block)
            ... 
            Block.WALL
            Block.WALL
            Block.EMPTY
            Block.WALL
    """
    _rows: list[Row[T]]

    __slots__ = {
        "_rows":
            "A list of rows.",
    }

    def __init__(self: Self, iterable: Iterable[Iterable[T]]) -> None:
        self._rows = [Row(row) for row in iterable]

    @overload
    def __getitem__(self: Self, index: tuple[int, int]) -> T: ...

    @overload
    def __getitem__(self: Self, index: int) -> Row[T]: ...

    @overload
    def __getitem__(
        self: Self, index: Union[tuple[int, slice], tuple[slice, int]]) -> Row[T]: ...

    @overload
    def __getitem__(
        self: Self, index: Union[slice, tuple[slice, slice]]) -> Layer[T]: ...

    def __getitem__(self, index):
        """Get an index or slice. Supports multi-indexing."""
        if isinstance(index, tuple) and len(index) != 2:
            raise TypeError(
                f"expected either layer[row] or layer[row, column]")
        elif isinstance(index, tuple):
            row, column = index
            if not isinstance(row, slice):
                return self._rows[row][column]
            elif isinstance(column, slice):
                return Layer(self._rows[i][column] for i in range(len(self))[row])
            else:
                return Row(self._rows[i][column] for i in range(len(self))[row])
        elif isinstance(index, slice):
            return Layer(self._rows[index])
        else:
            return self._rows[index]

    def __len__(self: Self) -> int:
        """The amount of rows in the layer."""
        return len(self._rows)

    def __iter__(self: Self) -> Iterator[T]:
        """Generates all of the rows in the layer."""
        return iter(self._rows)

    def __reversed__(self: Self) -> Iterator[T]:
        """Generates all of the rows in the layer in reversed order."""
        return reversed(self._rows)

    def __repr__(self: Self) -> str:
        """Code representation of the row."""
        return f"{type(self).__name__}({[row._blocks for row in self._rows]})"

    def __setitem__(self: Self, index: tuple[int, int], value: T) -> None:
        """Update an index."""
        row, column = index
        self._rows[row][column] = value

    @property
    def shape(self: Self) -> tuple[int, int]:
        """Returns (height, width)."""
        return (len(self._rows), len(self._rows[0]))

    @property
    def size(self: Self) -> int:
        """Returns height * width."""
        return len(self._rows) * len(self._rows[0])
