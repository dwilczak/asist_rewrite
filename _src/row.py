from __future__ import annotations

import sys
from typing import Any, Generic, Optional, TypeVar, overload

if sys.version_info < (3, 9):
    from typing import Iterable, Iterator, Sequence, List as list
else:
    from collections.abc import Iterable, Iterator, Sequence

from .block import Block

__all__ = ["Row"]

Self = TypeVar("Self", bound="Row")
T = TypeVar("T")


class Row(Sequence[T], Generic[T]):
    """
    Contains a row of blocks.

    Usage (like a list)
    --------------------
        Create a Row.
            >>> row = Row([Block.EMPTY, Block.WALL])
            >>> row
            Row([Block.EMPTY, Block.WALL])

        Index a block.
            >>> row[0]
            Block.EMPTY

        Update a block.
            >>> row[0] = Block.WALL
            >>> row
            Row([Block.WALL, Block.WALL])

        Iterate through the row.
            >>> for block in row:
            ...     print(block)
            ... 
            Block.WALL
            Block.WALL
    """
    _blocks: list[T]

    __slots__ = {
        "_blocks":
            "A list of blocks.",
    }

    def __init__(self: Self, iterable: Iterable[T]) -> None:
        self._blocks = [*iterable]

    @overload
    def __getitem__(self: Self, index: int) -> T: ...

    @overload
    def __getitem__(self: Self, index: slice) -> Row[T]: ...

    def __getitem__(self, index):
        """Get an index or slice."""
        if isinstance(index, slice):
            return Row(self._blocks[index])
        else:
            return self._blocks[index]

    def __len__(self: Self) -> int:
        """The amount of blocks in the row."""
        return len(self._blocks)

    def __iter__(self: Self) -> Iterator[T]:
        """Generates all of the blocks in the row."""
        return iter(self._blocks)

    def __reversed__(self: Self) -> Iterator[T]:
        """Generates all of the blocks in the row in reversed order."""
        return reversed(self._blocks)

    def __repr__(self: Self) -> str:
        """Code representation of the row."""
        return f"{type(self).__name__}({self._blocks!r})"

    def __setitem__(self: Self, index: int, value: T) -> None:
        """Update an index."""
        self._blocks[index] = value

    @property
    def shape(self: Self) -> tuple[int]:
        return (len(self),)

    @property
    def size(self: Self) -> int:
        return len(self)
