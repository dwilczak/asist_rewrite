import sys
from typing import Any, TypeVar

if sys.version_info < (3, 9):
    from typing import Iterable, Dict as dict
else:
    from collections.abc import Iterable

from .block import Block
from .map import Map
from .player import Player

__all__ = ["Game"]

Self = TypeVar("Self", bound="Game")


class Game:
    _map: Map[Block]
    _red_player: Player
    _green_player: Player
    _blue_player: Player

    def __init__(self: Self, iterable: Iterable[Iterable[Iterable[Block]]]) -> None:
        self._map = Map(iterable)
        self._red_player = Player(self._map)
        self._green_player = Player(self._map)
        self._blue_player = Player(self._map)

    def observe(self: Self, observation: dict[str, dict[str, Any]]) -> None:
        if ("message_type", "observation") in observation.get("header", {}).items():
            ...

    @property
    def map(self: Self) -> Map[Block]:
        return self._map
