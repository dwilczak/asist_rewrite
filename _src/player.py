from __future__ import annotations

import sys
from dataclasses import dataclass, field, InitVar
from typing import TypeVar

if sys.version_info < (3, 9):
    from typing import Iterator, Tuple as tuple
else:
    from collections.abc import Iterator

from .block import Block
from .map import Map

__all__ = ["Player"]

Self = TypeVar("Self", bound="Player")


@dataclass
class Player:
    map: InitVar[Map[Block]]
    x: float
    y: float
    z: float
    yaw: float
    pitch: float
    life: float = 20.0

    def __post_init__(self: Self, map: Map) -> None:
        self._seen: Map([
            [
                [False] * map.shape[2]
                for _ in map.shape[1]
            ]
            for _ in map.shape[0]
        ])

    def sees(self: Self, map: Map) -> None:
        """Updates the seen block positions based on the map."""
        raise NotImplementedError("TODO")

    @property
    def seen(self: Self) -> Map[bool]:
        """
        Returns a map of the seen blocks.

            >>> player.seen[0, 1, 2]
            True
        """
        return self._seen
