from enum import Enum
from typing import TypeVar

__all__ = ["Block"]

Self = TypeVar("Self", bound="Block")



class Block(Enum):
    """
    Defined blocks on the map.

    Usage
    ------
        Collect name and value of Block.
            >>> Block.WALL
            Block.WALL
            >>> Block.WALL.name
            'WALL'
            >>> Block.WALL.value
            'black'

        Index by attribute, name, or value.
            >>> Block.WALL.name
            'WALL'
            >>> Block["WALL"].name
            'WALL'
            >>> Block("black").name
            'WALL'

        Format the block for css.
            >>> Block.WALL.to_css()
            '???????'
    """
    EMPTY = "color"
    WALL = "black"

    def __repr__(self: Self) -> str:
        return f"Block.{self.name}"

    def to_css(self: Self) -> str:
        """Format the block for css."""
        return "???????"
